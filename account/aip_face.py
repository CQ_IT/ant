import base64

from aip import AipFace

""" 你的 APPID AK SK """
APP_ID = '14864763'
API_KEY = 'nuVFAeGUdbSfxmxiGdvilcz6'
SECRET_KEY = 'xHRFYeux22Hw0AIqsqPujavYWEP7nEZT'


def verify_img(avatar, img_list):
    client = AipFace(APP_ID, API_KEY, SECRET_KEY)
    for user, img in img_list:
        result = client.match([
            {
                'image': base64.b64encode([i for i in avatar.chunks()][0]).decode(),
                'image_type': 'BASE64',
            },
            {
                'image': base64.b64encode(open(img, 'rb').read()).decode(),
                'image_type': 'BASE64',
            }
        ])

        if result.get('error_code') != 0:
            return False
        return user


def verify_img1(img_path):
    client = AipFace(APP_ID, API_KEY, SECRET_KEY)

    result = client.match([
        {
            'image': base64.b64encode(open(img_path, 'rb').read()).decode(),
            'image_type': 'BASE64',
        },
        {
            'image': base64.b64encode(open('2.jpeg', 'rb').read()).decode(),
            'image_type': 'BASE64',
        }
    ])

    print(result)
    return result
