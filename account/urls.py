from django.urls import path
from rest_framework import routers

from account.views import *

router = routers.SimpleRouter()
router.register('users', UserViewSet, basename='Users')

urlpatterns = router.urls
urlpatterns += [
    path('identify/', identify),
]
