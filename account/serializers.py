from datetime import datetime, timedelta

from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken

from .models import *


class UserSerializer(serializers.ModelSerializer):
    pic = serializers.CharField(source='userprofile.pic', required=False)
    email = serializers.CharField(required=False)
    username = serializers.CharField(required=False)

    def validate_email(self, email):
        # 验证发送频率
        one_minutes_ago = datetime.now() - timedelta(hours=0, minutes=0, seconds=60)
        # 验证码添加时间大于一分钟以前的时间
        if VerificationCode.objects.filter(created_at__gt=one_minutes_ago, email=email).count():
            raise serializers.ValidationError("距离上一次发送未超过60S")

        # 判断手机号码是否注册过
        if UserProfile.objects.filter(email=email).count():
            raise serializers.ValidationError("该email已被注册")

        return email

    class Meta:
        model = User
        # fields = '__all__'
        exclude = ['user_permissions', 'groups', 'last_login', 'first_name', 'last_name',
                   'date_joined']


class PasswordSerializer(serializers.ModelSerializer):
    # 密码设置为掩码
    password = serializers.CharField(style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ('password',)


class UserRegSerializer(serializers.ModelSerializer):
    email = serializers.CharField(required=True)
    code = serializers.CharField(
        max_length=6, min_length=6, required=True,
        label="验证码",
        help_text='验证码',
        write_only=True,  # 不做序列化
        error_messages={
            'blank': '请输入验证码',
            'required': "请输入验证码",
            'max_length': "验证码格式错误",
            'min_length': "验证码格式错误"
        }
    )
    is_staff = serializers.BooleanField(required=False, default=False, help_text="是否是老师")
    password = serializers.CharField(required=True, write_only=True)
    result = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ['email', 'code', 'password', 'result', 'is_staff']

    def get_result(self, obj):
        refresh = RefreshToken.for_user(obj)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }

    def validate_code(self, code):
        verify_codes = VerificationCode.objects.filter(email=self.initial_data['email']).order_by("-created_at")
        if verify_codes:
            last_record = verify_codes.first()

            # 获得前五分钟
            five_minutes_ago = datetime.now() - timedelta(hours=0, minutes=5, seconds=0)
            # 判断最近的一次发送记录是否在五分钟之内 否在就验证码过期
            if five_minutes_ago > last_record.created_at:
                raise serializers.ValidationError("验证码过期")

            # 测试通用验证码
            if code != '123456':
                # 判断验证码是否正确
                if last_record.code != code:
                    raise serializers.ValidationError("验证码错误")
        else:
            raise serializers.ValidationError("请先发送验证码")

    def validate(self, attrs):
        """
        字段统一的处理
        :param attrs:
        :return:
        """
        attrs['username'] = attrs['email']
        del attrs['code']
        return attrs

    def create(self, validated_data):
        """
        重写create方法 为了加密密码
        """
        # 调用父类的创建方法
        user = super(UserRegSerializer, self).create(validated_data=validated_data)

        user.set_password(validated_data['password'])
        user.save()
        return user


class UserLoginSerializer(serializers.ModelSerializer):
    avatar = serializers.ImageField()

    class Meta:
        model = User
        fields = ['avatar']
