import os
import random
from email.header import Header
from email.mime.text import MIMEText
from smtplib import SMTP_SSL

from django.conf import settings
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response

from account.serializers import *


class UserViewSet(viewsets.ModelViewSet):
    """
    create：
        新增用户
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [IsAdminUser]
    search_fields = ['username']

    def get_permissions(self):
        """
            动态设置权限
        """
        if self.action in ['list', 'retrieve', 'destroy']:
            return [IsAdminUser()]
        elif self.action in ['set_password']:
            return [IsAuthenticated()]
        return []

    def get_serializer_class(self):
        if self.action in ['create']:
            return UserRegSerializer
        elif self.action in ['upload_avatar', 'login']:
            return UserLoginSerializer
        elif self.action in ['set_password']:
            return PasswordSerializer

        return self.serializer_class

    @action(detail=False, methods=['POST'])
    def sms_email(self, request):
        email = str(request.data.get('email'))
        try:
            email_from = "244245958@qq.com"  # 改为自己的发送邮箱
            email_to = email  # 接收邮箱
            hostname = "smtp.qq.com"  # 不变，QQ邮箱的smtp服务器地址
            login = "244245958@qq.com"  # 发送邮箱的用户名
            password = "jqoaqjnynvajcajg"  # 发送邮箱的密码，即开启smtp服务得到的授权码。注：不是QQ密码。
            subject = "python"  # 邮件主题
            li = []
            for i in range(6):
                li.append(random.randint(0, 9))
            num = ("".join('%s' % id for id in li))
            text = num  # 邮件正文内容

            smtp = SMTP_SSL(hostname)  # SMTP_SSL默认使用465端口
            smtp.login(login, password)

            msg = MIMEText(text, "plain", "utf-8")
            msg["Subject"] = Header(subject, "utf-8")
            msg["from"] = email_from
            msg["to"] = email_to
            smtp.sendmail(email_from, email_to, msg.as_string())
            smtp.quit()
            print("Success!")
            VerificationCode.objects.create(email=email, code=num)
            return Response({
                'code': 200,
                'msg': '发送成功'
            }, 200)
        except Exception as e:
            print('======================', e)
            print("Falied")
            return Response({
                'code': 400,
                'msg': '发送失败'
            }, 400)

    @action(detail=True, methods=['POST'])
    def set_password(self, request, pk=None):
        if self.request.user.is_staff:
            user = self.get_object()
        else:
            user = self.request.user
        serializer = PasswordSerializer(data=request.data)
        if serializer.is_valid():
            user.set_password(serializer.data['password'])
            user.save()
            return Response({
                'success': True,
                'msg': '密码修改成功'
            }, status=200)
        else:
            return Response({
                'success': False,
                'msg': '修改失败'
            }, status=400)

    @action(detail=True, methods=['POST'])
    def upload_avatar(self, request, pk=None):
        """
        上传视频 avatar: 图片
        """
        user = request.user
        if user.is_staff:
            try:
                instance = self.queryset.get(id=pk)
            except:
                return Response({
                    'success': False,
                    'msg': '参数不符合'
                }, 400)
        else:
            instance = self.get_object()
        file = request.FILES.get('avatar')
        # instance
        if file:

            pic = os.path.join(settings.AVATAR, file.name)
            with open(os.path.abspath(pic), 'wb') as f:
                for i in file.chunks():
                    f.write(i)
            instance.userprofile.pic = pic
            instance.userprofile.save()
            return Response({
                'success': True,
                'msg': '上传成功'
            }, 201)

        return Response({
            'success': False,
            'msg': '参数不符合'
        }, 400)

    @action(detail=False, methods=['POST'])
    def login(self, request):
        avatar = request.FILES.get('avatar')
        if not avatar:
            return Response({
                'success': False,
                'error': '401',
                'msg': '请传入一张图片'
            }, status=401)

        try:
            from .aip_face import verify_img
            img_list = [(i, i.userprofile.pic.path) for i in self.queryset if i.userprofile.pic]
            user = verify_img(avatar, img_list)
            if user:
                refresh = RefreshToken.for_user(user)
                return Response({
                    'refresh': str(refresh),
                    'access': str(refresh.access_token),
                }, status=200)
            return Response({
                'error': '401',
                'msg': '用户不存在'
            }, status=401)

        except User.DoesNotExist:
            return Response({
                'error': '401',
                'msg': '用户不存在'
            }, status=401)


@api_view(http_method_names=['GET'])
@permission_classes([IsAuthenticated])
def identify(request):
    """
    用于返回当前登录用户的信息
    :param request:
    :return: UserSerializer Object
    """
    data = UserSerializer(request.user)
    return Response(data.data)
