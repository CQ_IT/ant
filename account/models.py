from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    pic = models.ImageField(upload_to="face/", default=None, blank=True, null=True, help_text="人脸识别的图片")


class VerificationCode(models.Model):
    """
    验证码记录表
    """
    email = models.CharField(max_length=256, default="", null=True, blank=True, help_text="邮箱")
    code = models.CharField(max_length=10, default="", null=True, blank=True, help_text="验证码")
    created_at = models.DateTimeField(auto_now_add=True, help_text="发送时间")


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.userprofile.save()


