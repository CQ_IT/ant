from rest_framework import routers

from .views import *

router = routers.SimpleRouter()
router.register('topics', TopicViewSet, basename='Topic')
router.register('dialogues', DialogueViewSet, basename='Dialogue')

urlpatterns = router.urls
urlpatterns += [
]
