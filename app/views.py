import os
from datetime import datetime

from django.conf import settings
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response

from .serializers import *
from .test_demo.main import *


class TopicViewSet(viewsets.ModelViewSet):
    """
    题目表
    """
    serializer_class = TopicSerializer
    queryset = Topic.objects.all()
    permission_classes = [IsAdminUser]
    filter_fields = ['teacher', 'dialogue__user']

    def get_queryset(self):
        return self.queryset.distinct()

    def get_permissions(self):
        """
            动态设置权限
        """
        if self.action in ['retrieve']:
            return [IsAuthenticated()]
        elif self.action in ['list']:
            return []
        return [IsAdminUser()]


class DialogueViewSet(viewsets.ModelViewSet):
    """
    回答表
    """
    serializer_class = DialogueSerializer
    queryset = Dialogue.objects.all()
    permission_classes = [IsAuthenticated]
    filter_fields = ['topic', 'user']

    def create(self, request, *args, **kwargs):
        # 获取录音
        record = request.data.get('record')
        file = datetime.now().strftime('%y%m%d_%H%M%S') + '.mp3'
        result = dialogu_demo(record.read(), save_file=os.path.join(settings.ANSWER, file))
        if not result:
            return Response({
                'code': 400,
                'msg': '失败'
            }, 400)
        data = request.data
        data['answer'] = os.path.join('/media/record/answer', file)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(detail=False, methods=['PUT'])
    def evaluate(self, request):
        obj = self.get_object()
        point = self.request.get('point', None)
        evalution = self.request.get('point', None)
        if point and evalution:
            obj['point'] = point
            obj['evalution'] = evalution
            obj.save()
            return Response({
                'code': 200,
                'msg': '评分成功'
            }, 200)

    def perform_create(self, serializer):
        return serializer.save()

    def get_queryset(self):
        if self.request.user.is_staff:
            return self.queryset
        return self.queryset.filter(user=self.request.user)
