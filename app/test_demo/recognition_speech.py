# -*- coding: utf-8 -*-
# @Time     :2018/9/4
# @Author   :qpf

"""
对本地语音文件解析成文字
"""
from aip import AipSpeech


def voice2text(APP_ID, API_KEY, SECRET_KEY, file):
    client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)
    ret = client.asr(file, 'pcm', 16000, {'dev_pid': 1537}, )
    if ret.get('err_no', 1) == 0:
        return ret['result']
    return False

# def voice2text(APP_ID, API_KEY, SECRET_KEY, file_path):
#     client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)
#     ret = client.asr(get_data(file_path), 'pcm', 16000, {'dev_pid': 1537}, )
#     return ret['result']
#
#
# def get_data(file_path):
#     with open(file_path, 'rb') as fp:
#         return fp.read()
