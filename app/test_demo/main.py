# -*- coding: utf-8 -*-
# @Time     :2018/9/4
# @Author   :qpf

from translate import Translator

from . import compound_speech
from . import output_redio
from . import recognition_speech
from . import tuling_robot


def dialogu_demo(file, save_file):
    # 存放的文件名称
    # file_path = os.path.join(os.getcwd(), "tuling-answer.mp3")
    # file_path = os.path.join(os.getcwd(), "Mind.mp3")
    # 百度需要的参数
    APP_ID = '17956140'
    API_KEY = 'ypw0jazK4o6P9ILczUWKBXD9'
    SECRET_KEY = 'aoLCFNqD9PLf5WGioz1WNhDMZlDr6kXd'
    # 图灵需要的参数
    TULING_KEY = '2c0cca8d5010444593b46e3ca1e07915'
    # 先调用录音函数
    # input_record.record(file_path)
    # 语音转成文字的内容
    input_message = recognition_speech.voice2text(APP_ID, API_KEY, SECRET_KEY, file)
    if not input_message:
        return False

    print("输入的语音是：" + str(input_message[0]))
    # 英文翻译成中文
    translator = Translator(from_lang="english", to_lang="chinese")
    translation1 = translator.translate(input_message[0])
    # 获取回答信息
    answer = tuling_robot.answer(translation1, TULING_KEY)
    print(answer)
    # 存放回答的位置
    tuling_answer_path = save_file  # 语音存放的位置
    # 将文字转化为语音
    text = answer['text']
    translator = Translator(from_lang="chinese", to_lang="english")
    translation = translator.translate(answer['text'])
    print("中文回复是：" + text)
    print("输出的语音是：", translation)
    compound_speech.text2voice(APP_ID, API_KEY, SECRET_KEY, translation, tuling_answer_path)
    print(tuling_answer_path)
    # 播放图灵回答的内容
    output_redio.speak(tuling_answer_path)
    return tuling_answer_path
