from django.contrib.auth.models import User
from django.db import models


class Topic(models.Model):
    """
    题目表
    """
    teacher = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField(default=None, blank=True, null=True, help_text="题目")
    created_at = models.DateTimeField(auto_now_add=True, help_text="创建时间")


class Dialogue(models.Model):
    """
    对话表
    """
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    record = models.FileField(upload_to='record/dialogue', default=None, blank=True, null=True, help_text="对话")
    answer = models.CharField(max_length=256, default=None, blank=True, null=True, help_text="回答")
    point = models.IntegerField(default=0, blank=True, null=True, help_text="评分")
    evalution = models.CharField(max_length=256, default=None, blank=True, null=True, help_text="评价")
    created_at = models.DateTimeField(auto_now_add=True, help_text="创建时间")
