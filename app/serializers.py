from rest_framework import serializers

from .models import *


class DialogueSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    play_answer = serializers.SerializerMethodField(read_only=True, required=False)
    created_at = serializers.DateTimeField(read_only=True, format='%Y-%m-%d %H:%M')


    def get_play_answer(self, obj):
        if obj.answer:
            request = self.context['request']
            return request.META['wsgi.url_scheme'] + '://' + request.META['HTTP_HOST'] + str(obj.answer)
        return obj.answer

    class Meta:
        model = Dialogue
        fields = ['id', 'user', 'play_answer', 'record', 'point', 'answer', 'evalution', 'created_at', 'topic']


class TopicSerializer(serializers.ModelSerializer):
    teacher = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    dialogue_set = DialogueSerializer(many=True)
    created_at = serializers.DateTimeField(read_only=True, format='%Y-%m-%d %H:%M')
    created_user = serializers.SerializerMethodField(read_only=True)

    dialogue_count = serializers.SerializerMethodField(read_only=True)

    def get_dialogue_count(self, obj):
        request = self.context['request']
        user = request.user
        if user.is_active:
            return obj.dialogue_set.filter(user=user).count()
        return None

    def get_created_user(self, obj):
        from account.serializers import UserSerializer
        return UserSerializer(obj.teacher).data

    class Meta:
        model = Topic
        fields = ['id', "teacher", "dialogue_set", "created_at", "content", "created_user", "dialogue_count"]
